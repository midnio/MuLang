# -*- encoding:utf-8 -*-
from sys import argv
from os import system
""" Lexer """
class Lex:
    def __init__(self, tolex):
        self.text = tolex
        self.toks = list()
        self.vars = dict()
    def l(self):
        tok, string, instr, exp, inexp, invar, var = "", "", False, "", False, False, ""
        for char in self.text:
            tok += char
            if  "code:" == tok or "main:" == tok:
                self.toks.append("CODE")
                tok = ""
            elif '\n' == tok:
                if inexp and exp != "":
                    self.toks.append("E:"+exp)
                    exp = ""
                elif not inexp and exp != "":
                    self.toks.append("N:"+exp)
                    exp = ""
                elif var != "":
                    self.toks.append("V:"+var)
                    var = ""
                    invar = False
                tok = ""
            elif "\t" == tok:
                tok = ""
            elif ":" == tok and not instr:
                if var != "":
                    self.toks.append("V:"+var)
                    var = ""
                    invar = False
                self.toks.append("EQUAL")
                tok = ""
            elif "is" == tok and not instr:
                if not inexp and exp != "":
                    self.toks.append("N:"+exp)
                    exp = ""
                self.toks.append("EQQ")
                tok = ""
            elif "&" == tok and not instr:
                invar = True
                var += tok
                tok = ""
            elif invar:
                var += tok
                tok = ""
            elif tok.isspace() and not instr:
                tok = ""
            elif tok.isnumeric():
                exp += tok
                tok = ""
            elif tok in "+%/()*-":
                inexp = True
                exp += tok
                tok = ""
            elif tok.lower() == "sys":
                self.toks.append("sys")
                tok = ""
            elif "echo" == tok.lower():
                self.toks.append("ECHO")
                tok = ""
            elif "if" == tok.lower():
                self.toks.append("IF")
                tok = ""
            elif "eni" == tok.lower():
                self.toks.append("ENDIF")
                tok = ""
            elif "do" == tok.lower():
                if not inexp and exp != "":
                    self.toks.append("N:"+exp)
                    exp = ""
                self.toks.append("DO")
                tok = ""
            elif "get" == tok.lower():
                self.toks.append("GET")
                tok = ""
            elif '"' == tok or ' "' == tok:
                if not instr:
                    instr = True
                elif instr:
                    self.toks.append("S:"+string+'"')
                    string = ""
                    tok = ""
                    instr = False
            elif instr:
                string += tok
                tok = ""
        print(self.toks)
    def p(self):
        i = 0
        while i < len(self.toks):
            if self.toks[i] == "ENDIF":
                i += 1
            elif self.toks[i] == "ECHO":
                if self.toks[i+1][:1] == "S":
                    print(self.toks[i+1][2:][1:][:-1])
                    i += 2
                elif self.toks[i+1][:1] == "N":
                    print(self.toks[i+1][2:])
                    i += 2
                elif self.toks[i+1][:1] == "E":
                    print(eval(self.toks[i+1][2:]))
                    i += 2
                elif self.toks[i+1][:1] == "V":
                    try:
                        print(self.vars[self.toks[i+1][2:]])
                    except KeyError:
                        print("NoVarErr: Undefined variable or variable used before assigning.")
                    i += 2
                else:
                    pass
            elif self.toks[i][:1] + " " + self.toks[i+1] == "V EQUAL":
                if self.toks[i+2][:1] == "S":
                    self.vars[self.toks[i][2:]] = self.toks[i+2][2:]
                    i += 3
                elif self.toks[i+2][:1] == "N":
                    self.vars[self.toks[i][2:]] = int(self.toks[i+2][2:])
                    i += 3
                elif self.toks[i+2][:1] == "E":
                    self.vars[self.toks[i][2:]] = eval(self.toks[i+2][2:])
                    i += 3
                elif self.toks[i+2][:1] == "V":
                    self.vars[self.toks[i][2:]] = self.vars[self.toks[i+2][2:]]
                    i += 3
            elif self.toks[i][:3] + " " + self.toks[i+1][:1] + " " + self.toks[i+2][:1] == "GET S V":
                self.vars[self.toks[i+2][2:]] = str(input(self.toks[i+1][2:][1:-1]))
                i += 3
            elif self.toks[i] == "IF":
                if self.toks[i+1][:1] + " " + self.toks[i+2] + " " + self.toks[i+3][:1] + " " + self.toks[i+4] == "N EQQ N DO":
                    if self.toks[i+1][2:] == self.toks[i+3][2:]:
                        print("True")
                    else:
                        print("False")
                    i+=5
            elif self.toks[i] == "sys":
                system(self.toks[i+1][1:-1])
            else:
                print('\033[91m'+"UnkFuncErr: Unknown Func.")
                break
""" Getting data from file """
with open(argv[1], "r") as f:
    data = f.read()
    data += "\n"
lex = Lex(data)
lex.l()
lex.p()
